/**
 * Created by Seng Ghee on 21/11/2016.
 */


(function () {
    angular
        .module("DataTableApp")
        .controller("b3Ctrl", b3Ctrl);

    b3Ctrl.$inject = ["$http","$state"];

    function b3Ctrl($http,$state) {
        console.log ("In B3 controller")
    }

})();