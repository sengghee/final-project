/**
 * Created by Seng Ghee on 21/11/2016.
 */


(function () {
    angular
        .module("DataTableApp")
        .controller("b2Ctrl", b2Ctrl);

    b2Ctrl.$inject = ["$http","$state"];

    function b2Ctrl($http,$state) {
        console.log ("In B2 controller")
    }

})();