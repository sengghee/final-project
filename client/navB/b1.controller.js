/**
 * Created by Seng Ghee on 21/11/2016.
 */


(function () {
    angular
        .module("DataTableApp")
        .controller("b1Ctrl", b1Ctrl);

    b1Ctrl.$inject = ["$http","$state"];

    function b1Ctrl($http,$state) {

        console.log ("In B1 controller")
    }

})();