/**
 * Created by Seng Ghee on 21/11/2016.
 */

(function () {
    angular
        .module("DataTableApp")
        .controller("c2Ctrl", c2Ctrl);

    c2Ctrl.$inject = ["$http","$state"];

    function c2Ctrl($http,$state) {
        console.log ("In C2 controller")
    }

})();