/**
 * Created by Seng Ghee on 21/11/2016.
 */

(function () {
    angular
        .module("DataTableApp")
        .controller("c3Ctrl", c3Ctrl);

    c3Ctrl.$inject = ["$http","$state"];

    function c3Ctrl($http,$state) {

        console.log ("In C3 controller")
    }

})();