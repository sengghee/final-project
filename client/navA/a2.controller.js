/**
 * Created by Seng Ghee on 21/11/2016.
 */

(function () {
    angular
        .module("DataTableApp")
        .controller("a2Ctrl", a2Ctrl);

    a2Ctrl.$inject = ["$http","$state"];

    function a2Ctrl($http,$state) {
        console.log ("In A2 controller")
    }

})();