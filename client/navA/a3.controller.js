/**
 * Created by Seng Ghee on 21/11/2016.
 */

(function () {
    angular
        .module("DataTableApp")
        .controller("a3Ctrl", a3Ctrl);

    a3Ctrl.$inject = ["$http","$state"];

    function a3Ctrl($http,$state) {

        console.log ("In A3 controller")
    }

})();