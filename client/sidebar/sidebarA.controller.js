/**
 * Created by Seng Ghee on 21/11/2016.
 */


(function () {
    angular
        .module("DataTableApp")
        .controller("sidebarACtrl", sidebarACtrl);

    sidebarACtrl.$inject = ["$http","$state"];

    function sidebarACtrl($http,$state) {

        localStorage.setItem("content", "A.A1");

        var contentDisplay = localStorage.getItem("content");

        console.log("In sidebarA.controller.js the contentDisplay is " + contentDisplay);

        $state.go(contentDisplay);

    }

})();