/**
 * Created by Seng Ghee on 21/11/2016.
 */


(function () {
    angular
        .module("DataTableApp")
        .controller("sidebarBCtrl", sidebarBCtrl);

    sidebarBCtrl.$inject = ["$http","$state"];

    function sidebarBCtrl($http,$state) {

        localStorage.setItem("content", "B.B1");

        var contentDisplay = localStorage.getItem("content");

        console.log("In sidebarB.controller.js local storage content " + contentDisplay);

        $state.go(contentDisplay);
    }

})();