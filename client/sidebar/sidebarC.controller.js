/**
 * Created by Seng Ghee on 22/11/2016.
 */

(function () {
    angular
        .module("DataTableApp")
        .controller("sidebarCCtrl", sidebarCCtrl);

    sidebarCCtrl.$inject = ["$http","$state"];

    function sidebarCCtrl($http,$state) {

        localStorage.setItem("content", "C.C1");

        var contentDisplay = localStorage.getItem("content");

//        console.log("In sidebarC.controller.js local storage content " + contentDisplay);

        $state.go(contentDisplay);
    }

})();