(function () {
    angular
        .module("DataTableApp")
        .config(datatableAppConfig)
    datatableAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function datatableAppConfig($stateProvider,$urlRouterProvider){

// Parent
            $stateProvider
                .state('A',{
                    url : '/A',
                    templateUrl :'./sidebar/sidebarA.html',
                    controller : 'sidebarACtrl',
                    controllerAs : 'ctrl'
                })
                .state('A.A1',{
                    url : '/A1',
                    templateUrl :'./navA/a1.html',
                    controller : 'a1Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('A.A2',{
                    url : '/A2',
                    templateUrl :'./navA/a2.html',
                    controller : 'a2Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('A.A3',{
                    url : '/A3',
                    templateUrl :'./navA/a3.html',
                    controller : 'a3Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('B',{
                    url : '/B',
                    templateUrl :'./sidebar/sidebarB.html',
                    controller : 'sidebarBCtrl',
                    controllerAs : 'ctrl'
                })
                .state('B.B1',{
                    url : '/B1',
                    templateUrl :'./navB/b1.html',
                    controller : 'b1Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('B.B2',{
                    url : '/B2',
                    templateUrl :'./navB/b2.html',
                    controller : 'b2Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('B.B3',{
                    url : '/B3',
                    templateUrl :'./navB/b3.html',
                    controller : 'b3Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('B.B4',{
                    url : '/B4',
                    templateUrl :'./navB/b4.html',
                    controller : 'b4Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('C',{
                    url : '/C',
                    templateUrl :'./sidebar/sidebarC.html',
                    controller : 'sidebarCCtrl',
                    controllerAs : 'ctrl'
                })
                .state('C.C1',{
                    url : '/C1',
                    templateUrl :'./navC/c1.html',
                    controller : 'c1Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('C.C2',{
                    url : '/C2',
                    templateUrl :'./navC/c2.html',
                    controller : 'c2Ctrl',
                    controllerAs : 'ctrl'
                })
                .state('C.C3',{
                    url : '/C3',
                    templateUrl :'./navC/c3.html',
                    controller : 'c3Ctrl',
                    controllerAs : 'ctrl'
                })

        localStorage.setItem("content", "/A/A1");

//      console.log("In navtop js before var contentDisoplay" + contentDisplay);

        var contentDisplay = localStorage.getItem("content") ;

//       console.log ("after var contentDisplay " + contentDisplay ) ;*/

        $urlRouterProvider.otherwise(contentDisplay);
        }
})();