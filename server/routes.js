/**
 * Created by vishnu on 30/10/16.
 */

var CardTypeController = require ('./api/cardtype/cardtype.controller');

function initRouteHandler(app) {

    app.post("/api/cardType", CardTypeController.search);
}

function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });

}

module.exports = {
    init: initRouteHandler,
    errorHandler: errorHandler
}
