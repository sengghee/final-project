var Sequelize = require("sequelize");
var config = require('./config');

//Create a connection with mysql DB
var sequelize = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password,
    {
        host: config.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        },
        dialectOptions: {
            insecureAuth: true
        }
    });

sequelize.authenticate().then(function(errors) {
    if (!!errors) {
        console.log('Unable to connect to the database:', err)
    } else {
        console.log('Connection has been established successfully.')
    }
});

sequelize.sync().then(function () {
    console.log("Database in Sync Now")
});

var CardTypeModal = require('./model/cardtype')(sequelize, Sequelize);

console.log ("end of database.js");

module.exports={
    CardType : CardTypeModal
}