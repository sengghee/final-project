/**
 * Created by Seng Ghee on 4/12/2016.
 */

module.exports = function (sequelize, Sequelize) {
    var CardType = sequelize.define('gs_acc_cardtype', {
        int_ID: {
            type:Sequelize.INTEGER,
            primaryKey: true
        },
        int_CardTypeID: Sequelize.INTEGER,
        vc_Name: Sequelize.STRING,
        vc_ProductNo:Sequelize.STRING,
        ti_FuncType:Sequelize.INTEGER,
        ti_Mode:Sequelize.INTEGER,
        ti_ValidPeriodFlag:Sequelize.INTEGER,
        ti_SellFlag:Sequelize.INTEGER,
        ti_IvrFlag:Sequelize.INTEGER,
        ti_Deposit:Sequelize.INTEGER,
        int_DepositTime:Sequelize.INTEGER,
        f_DepositMoney:Sequelize.FLOAT,
        int_PhoneMax:Sequelize.INTEGER,
        ti_RealFlag:Sequelize.INTEGER,
        f_RealScale:Sequelize.FLOAT,
        ti_AutoFlag:Sequelize.INTEGER,
        vc_Publish:Sequelize.STRING,
        vc_Agent:Sequelize.STRING,
        vc_Remark:Sequelize.STRING,
        dt_CreateTime: Sequelize.DATE,
        dt_EditTime: Sequelize.DATE
    }, {
        timestamps: false,
        freezeTableName: true,

        // define the table's name
        tableName: 'gs_acc_cardtype'

    });
    console.log("End of cardtype database populate");
    return CardType;
}