

//Load express
var express = require("express");
//Create an instance of express application
var config=require('./config');
var routes = require("./routes");

var app = express();

var bodyParser = require("body-parser");


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */

app.use(express.static(__dirname + "/../client/"));
routes.init(app);

//Start the web server on port 3000
app.listen(config.port, function () {
    console.info("Webserver started on port " + config.port);
});
